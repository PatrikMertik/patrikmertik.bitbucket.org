
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
            "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi data (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */


function preberiMeritve(){
    
    sessionId = getSessionId();
    
    var ehrId = $("#addEHR").val();
    
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var party = data.party;
    //    $("#header").append("Weight measurements for " + party.firstNames + ' ' +    party.lastNames);
     }
        });
    

    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (resWeight) {
        
        
        
        
       
            
            
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (resHeight) {

        $("#visina").empty();
        $("#visina").append(resHeight[0].height);
        
        $("#teza").empty();
        $("#teza").append(resWeight[0].weight);
        //for (var i in res) {
          //  $("#result").append(res[i].time + ': ' + res[i].weight + res[i].unit + "<br>");
                      }
                }
            );}
      });
}






// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function izracun(){

    var info = document.getElementById("fri");
    var teza = info.elements["teza"].value;
    var visina = info.elements["visina"].value;
    var IBM = teza/ Math.pow(visina/100, 2);
    if(teza != "" && visina != ""){
        IBM = IBM.toFixed(3);
        document.getElementById("cefur").innerHTML = IBM;
        
        
            var svg=d3.select("svg");
              var g=svg.append("g").attr("transform","translate(180,140)");
              var domain = [0,100];  
              var gg = viz.gg()
                .domain(domain)
                .outerRadius(120)
                .innerRadius(12)
                .value(IBM)
                .duration(1000);
              gg.defs(svg);
              g.call(gg);  
    }else{
        document.getElementById("cefur").innerHTML = "Polja ne smejo biti prazna!";
    }

     
}

var info = document.getElementById("fri");
    var teza = info.elements["teza"].value;
    var visina = info.elements["visina"].value;
    var IBM = teza/ Math.pow(visina/100, 2);
var svg=d3.select("svg");
              var g=svg.append("g").attr("transform","translate(180,140)");
              var domain = [0,100];
              
              var gg = viz.gg()
                .domain(domain)
                .outerRadius(120)
                .innerRadius(12)
                .value(IBM)
                .duration(1000);
              
              gg.defs(svg);
              g.call(gg);  
              
              gg.setNeedle();



              d3.select(self.frameElement).style("height", "280px");

              $("#cefur").empty();
              $("#cefur").append("<b>"+IBM+"</b>");
       

